// : vi ts=4 sw=4 noet :
/*
# --------------------------------------------------------------------------
# Mnemonic:	colour.go
# Abstract:	A colour library
#
# Date:		2 January 2017
# Author:	E. Scott Daniels
#
# --------------------------------------------------------------------------
*/

package colour

import (
	"fmt"
	"gitlab.com/rouxware/goutils/clike"
)

const (
	AQUA         int = 0x00FFFF
	BLACK        int = 0x000000
	BLUE         int = 0x0000FF
	BLUEVIOLET   int = 0xa600ff
	BROWN        int = 0x996600
	BROWNSAND    int = 0xc6b482
	BURNTORANGE  int = 0xdca613
	CHOCOLATE    int = 0x53401c
	COCOA        int = 0x976556
	DARKGREY     int = 0x303030
	DEEPPURPLE   int = 0xab359c
	FUSCHIA      int = 0xFF00FF
	GRAY         int = 0x808080
	GREY         int = 0x808080
	GREYBLUE     int = 0xd2cdff
	GREYBROWN    int = 0xded1b9
	GREEN        int = 0x008000
	INDAGO       int = 0x330099
	LATTE        int = 0xCD921C
	LAVENDER     int = 0xaea6f6
	LCDBG        int = 0xe0e0c8 // similar to calculator background
	LTGREEN      int = 0x00C800
	LTBLUE       int = 0x0000C8
	LTRED        int = 0xC80000
	LIME         int = 0x00FF00
	MAGENTA      int = 0xCC3399
	MAROON       int = 0x800000
	MIDNIGHTBLUE int = 0x6b46a9
	MUSTARD      int = 0xEFDD3D
	NAVY         int = 0x000080
	OLIVE        int = 0x808000
	ORANGE       int = 0xFF9900
	PIGPINK      int = 0xFF99F1
	PINK         int = 0xff80ff
	PURPLE       int = 0x800080
	RED          int = 0xFF0000
	SAND         int = 0xFFF3D2
	SEAGREEN     int = 0x02a090
	SILVER       int = 0xC0C0C0
	STEELBLUE    int = 0x726d7b
	TAN          int = 0xFFDA91
	TEAL         int = 0x008080
	VIOLET       int = 0xB000FF
	WHITE        int = 0xFFFFFF
	YELLOW       int = 0xFFFF00
	YELLOWBUS    int = 0xFEE233
	YELLOWGREEN  int = 0xD2FF91
)

var (
	name2cvalue map[string]int
)

func init() {
	name2cvalue = make(map[string]int, 128)

	name2cvalue["aqua"] = AQUA
	name2cvalue["black"] = BLACK
	name2cvalue["blue"] = BLUE
	name2cvalue["blueviolet"] = BLUEVIOLET
	name2cvalue["brown"] = BROWN
	name2cvalue["brownsand"] = BROWNSAND
	name2cvalue["burntorange"] = BURNTORANGE
	name2cvalue["chocolate"] = CHOCOLATE
	name2cvalue["cocoa"] = COCOA
	name2cvalue["darkgrey"] = DARKGREY
	name2cvalue["deeppurple"] = DEEPPURPLE
	name2cvalue["fuschia"] = FUSCHIA
	name2cvalue["gray"] = GRAY
	name2cvalue["grey"] = GREY
	name2cvalue["greyblue"] = GREYBLUE
	name2cvalue["greybrown"] = GREYBROWN
	name2cvalue["indago"] = INDAGO
	name2cvalue["green"] = GREEN
	name2cvalue["latte"] = LATTE
	name2cvalue["lavender"] = LAVENDER
	name2cvalue["lcdbg"] = LCDBG
	name2cvalue["ltgreen"] = LTGREEN
	name2cvalue["ltblue"] = LTBLUE
	name2cvalue["ltred"] = LTRED
	name2cvalue["lime"] = LIME
	name2cvalue["magenta"] = MAGENTA
	name2cvalue["maroon"] = MAROON
	name2cvalue["midnightblue"] = MIDNIGHTBLUE
	name2cvalue["mustard"] = MUSTARD
	name2cvalue["navy"] = NAVY
	name2cvalue["olive"] = OLIVE
	name2cvalue["orange"] = ORANGE
	name2cvalue["pigpink"] = PIGPINK
	name2cvalue["pink"] = PINK
	name2cvalue["purple"] = PURPLE
	name2cvalue["red"] = RED
	name2cvalue["sand"] = SAND
	name2cvalue["seagreen"] = SEAGREEN
	name2cvalue["silver"] = SILVER
	name2cvalue["steelblue"] = STEELBLUE
	name2cvalue["tan"] = TAN
	name2cvalue["teal"] = TEAL
	name2cvalue["violet"] = VIOLET
	name2cvalue["white"] = WHITE
	name2cvalue["yellow"] = YELLOW
	name2cvalue["yellowbus"] = YELLOWBUS
	name2cvalue["yellowgreen"] = YELLOWGREEN
}

/*
	String2colour will convert a string into a colour value. String may be one of:
		0xrrggbb
		#rrggbb
		colour-name
*/
func string2colour(str string) int {
	if str == "" {
		str = "grey"
	}

	if str[0:2] == "0x" {
		return clike.Atoi(str)
	} else {
		if str[0:1] == "x" {
			return clike.Atoi("0" + str)
		} else {
			if str[0:1] == "#" {
				return clike.Atoi("0x" + str[1:])
			} else {
				return name2cvalue[str]
			}
		}
	}
}

type Colour struct {
	str   string // the name or 0x string which was used to create it
	cval  int    // composite value
	red   int    // individual values
	green int
	blue  int
}

/*
	Internal function to set the composite value and string based on the rgb.
*/
func (c *Colour) set_cs() {
	c.cval = (c.red << 16) + (c.green << 8) + c.blue
	c.str = fmt.Sprintf("0x%02x%02x%02x", c.red, c.green, c.blue)
}

func Mk_colour(arg interface{}) (c *Colour) {
	c = &Colour{}

	c.Set_colour(arg)

	return c
}

/*
	Set the colour represented by the struct. Arg may be a string (0xrrggbb, or name) or an array of int (r, g, b).
*/
func (c *Colour) Set_colour(arg interface{}) {
	if c == nil {
		return
	}

	switch thing := arg.(type) {
	case string:
		c.str = thing
		c.cval = string2colour(thing)
		cv := c.cval
		c.blue = cv % 256

		cv = cv >> 8
		c.green = cv % 256

		cv = cv >> 8
		c.red = cv % 256

	case []int:
		if len(thing) >= 3 {
			c.red = thing[0] % 255
			c.green = thing[1] % 255
			c.blue = thing[2] % 255
			c.set_cs() // set the composite and string
		}
	}
}

/*
	Lighten the colour by a given percentage. Pct may be an integer (1-100) or a float64 which will be
	used as a direct multiplier.
*/
func (c *Colour) Lighten(pct interface{}) {
	var p float64
	if c == nil {
		return
	}

	switch up := pct.(type) {
	case float64:
		if up > 1 {
			p = 1.0
		} else {
			if up < 0 {
				p = 0.0
			} else {
				p = up
			}
		}

	case int:
		if up > 100 {
			p = 1.0
		} else {
			if up < 0 {
				p = 0.0
			} else {
				p = float64(up) / 255.0
			}
		}

	}

	c.red += int(float64(c.red) * p)
	c.green += int(float64(c.green) * p)
	c.blue += int(float64(c.blue) * p)
	c.set_cs() // set new composite and string
}

/*
	Darken the colour by a given percentage. Pct may be an integer (1-100) or a float64 which will be
	used as a direct multiplier.
*/
func (c *Colour) Darken(pct interface{}) {
	var p float64

	if c == nil {
		return
	}

	switch up := pct.(type) {
	case float64:
		if up > 1 {
			p = 1.0
		} else {
			if up < 0 {
				p = 0.0
			} else {
				p = up
			}
		}

	case int:
		if up > 100 {
			p = 1.0
		} else {
			if up < 0 {
				p = 0.0
			} else {
				p = float64(up) / 255
			}
		}

	}

	c.red -= int(float64(c.red) * p)
	if c.red < 0 {
		c.red = 0
	}
	c.green -= int(float64(c.green) * p)
	if c.green < 0 {
		c.green = 0
	}
	c.blue -= int(float64(c.blue) * p)
	if c.blue < 0 {
		c.blue = 0
	}
	c.set_cs() // compute the composite value and string from the new rgb settings
}

/*
	Invert the coluor
*/
func (c *Colour) Invert() {
	if c == nil {
		return
	}

	c.red = 256 - c.red
	c.green = 256 - c.green
	c.blue = 256 - c.blue
	c.set_cs()
}

/*
	Return the composite value as an integer.
*/
func (c *Colour) Get_composite() int64 {
	if c == nil {
		return 0
	}

	return int64(c.cval)
}

/*
	Return the red green and blue values.
*/
func (c *Colour) Get_rgb() (int, int, int) {
	if c == nil {
		return 0, 0, 0
	}

	return c.red, c.green, c.blue
}

/*
	Return the red green and blue values as floating percentages of 255.
*/
func (c *Colour) Get_rgb_pct() (float64, float64, float64) {
	if c == nil {
		return 0.0, 0.0, 0.0
	}

	return float64(c.red) / 255.0, float64(c.green) / 255.0, float64(c.blue) / 255.0
}

/*
	Implement the stringer interface.
*/
func (c *Colour) String() string {
	if c == nil {
		return "<nil-colour>"
	}

	return c.str
}
