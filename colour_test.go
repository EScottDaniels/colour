package colour_test

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/EScottDaniels/colour"
)

/*
funct init( ) {
func get_colour_by_name( name ) ( int ) {
func string2colour( str string ) ( int ) {
	Internal function to set the composite value and string based on the rgb.
func( c *Colour ) set_cs() {
func ( c *Colour )  Set_colour( arg interface{} ) {
func ( c *Colour) Lighten(  pct interface{} ) {
func ( c *Colour) Darken(  pct interface{} ) {
func ( c *Coluor ) Invert() {
func ( c *Colour ) Get_rgb() ( int, int, int ) {
func ( c *Colour ) Get_rgb_pct() ( float64, float64, float64) {
func ( c *Colour ) String() ( string ) {
*/

func TestColour(t *testing.T) {
	co := colour.Mk_colour("blueviolet")
	r, g, b := co.Get_rgb()
	fr, fg, fb := co.Get_rgb_pct()
	fmt.Fprintf(os.Stderr, ">>> %d %d %d == %s\n", r, g, b, co)
	fmt.Fprintf(os.Stderr, ">>> %.2f %.2f %.2f == %s\n", fr, fg, fb, co)

	co = colour.Mk_colour("seagreen")
	//co.Darken( 0.50 )
	r, g, b = co.Get_rgb()
	fr, fg, fb = co.Get_rgb_pct()
	fmt.Fprintf(os.Stderr, ">>> %x %x %x == %s\n", r, g, b, co)
	fmt.Fprintf(os.Stderr, ">>> %.2f %.2f %.2f == %s\n", fr, fg, fb, co)

	step := fb
	if fr > fg {
		if fr > fb {
			step = fr
		}
	} else {
		if fg > fb {
			step = fg
		}
	}
	fmt.Fprintf(os.Stderr, ">>> step = %.04f\n", step)
	step = (1 - step) / 21.0
	fmt.Fprintf(os.Stderr, ">>> step = %.04f\n", step)
	for i := 0; i < 20; i++ {
		fr += step
		fg += step
		fb += step
		fmt.Fprintf(os.Stderr, ">>> %.3f %.3f %.3f\n", fr, fg, fb)
	}

}
